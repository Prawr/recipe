# TODO list
- Best size for images is 3x2(1800x1200) (App - Detail)

## Design
- **Ingredients list right-left to left-right (App - Detail)**
- **Fixed max size for images (App - Detail)**
- **Fix height issue with containers for the categories (App - Home)**
- *Go over from Flex to Grid CSS (App/CMS)*
- *Reduce redundancy in CSS*


## Functionality
- Think of solution for recipes that require 3 different lists of ingredients for their components (App - Detail)
- Add the editing of items (CMS)
- Add offline access (App)


## Quality of Life
- Click to enlarge image (App - Detail)
- Figure out how to properly show categories and recipes if there are more than 50 items (CMS)
- **Enable 'Enter' for adding items in addition to clicking 'Add' (CMS)**


## Lifetime
- Check code quality and efficiency
- Add tests
- Optimize CSS

