// Main import
import { assert } from 'chai';

// Component import
import categories, { TOGGLE_ACTIVE } from '../../src/reducers/categories';

// Test suite
describe('reducer.category', () => {
  it('should return the initial state', () => {
    // Arrange
    const state = undefined;
    const action = {};
    const expectedResult = {};
    // Act
    const result = categories(state, action);
    // Assert
    assert.deepEqual(result, expectedResult);
  });

  it('should return the new state', () => {
    // Arrange
    const category = {
      name: 'Favourites',
      active: false
    }
    const state = {
      categories: [
        {
          name: 'Favourites',
          active: false
        }
      ]
    };
    const action = {
      type: 'TOGGLE_ACTIVE',
      category
    };
    const expectedResult = [
      {
        name: 'Favourites',
        active: true
      }
    ];
    // Act
    const result = categories(state, action);
    // Assert
    assert.deepEqual(result, expectedResult);
  });
});
