// Main import
import React, { Component } from 'react';

// Component import

// CSS import
import './summary.css';

// Component
const EntityList = ({category, recipe, selectEntity}) => (
	<div className="aside entity-list">
		<a className={category ? "button entity-button active" : "button entity-button"} onClick={() => selectEntity('category')}>Categories</a>
		<a className={recipe ? "button entity-button active" : "button entity-button"} onClick={() => selectEntity('recipe')}>Recipes</a>
	</div>
);

export default EntityList;
