// Main import
import React, { Component } from 'react';

// Component import
import EntityList from './EntityList';
import ItemList from './ItemList';
import CategoryContent from '../creadit/CategoryContent';
import RecipeContent from '../creadit/RecipeContent';

// Component
class Summary extends Component {
  state = {
		summary: true,
    category: true,
		recipe: false,
		currentItem: null,
	}
	
	selectEntity(entity){
		if(entity==='category'){
			this.setState({category: true, recipe: false});
		} else if(entity==='recipe'){
			this.setState({category: false, recipe: true});
		}
	}

	selectItem(operation, item = undefined){
		if(operation==='create'){
			this.setState({summary: false});
		} else if(operation==='update'){
			this.setState({summary: false, currentItem: item});
		} else {
			this.setState({summary: true});
		}
	}

	goBack(){
		this.setState({summary: true, currentItem: null});
	}

	render(){
		const { categories, recipes, saveCategory, saveRecipe } = this.props;
		if(this.state.summary==true){
			return (
				<div className="summarypage">
					<EntityList category={this.state.category} recipe={this.state.recipe} selectEntity={this.selectEntity.bind(this)} />
					<ItemList category={this.state.category} recipe={this.state.recipe} categories={categories} recipes={recipes} selectItem={this.selectItem.bind(this)} />
				</div>
			);
		} else if(this.state.summary==false && this.state.category==true){
			return (
				<div className="summarypage">
					<CategoryContent saveCategory={saveCategory} oldCategory={this.state.currentItem} goBack={this.goBack.bind(this)} />
				</div>
			)
		} else if(this.state.summary==false && this.state.recipe==true){
			return (
				<div className="summarypage">
					<RecipeContent saveRecipe={saveRecipe} oldRecipe={this.state.currentItem} goBack={this.goBack.bind(this)} allCategories={categories} />
				</div>
			)
		}
	}
};

export default Summary;
