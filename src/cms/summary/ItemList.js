// Main import
import React, { Component } from 'react';

// Component import

// CSS import
import './summary.css';

// Component
class ItemList extends Component {
	render(){
		const { categories, recipes, category, recipe, selectItem } = this.props;
		const header = category==true ? 'Categories' : 'Recipes';

		return (
			<div className="content item-list">
				<h2 className="itemlist-header">{header}</h2>
				<a className="itemlist-new-button" onClick={() => selectItem('create')}>New</a>
				<div className="itemlist-underline"></div>
				<div className="itemlist-content">
					{category==true ? 
						categories.map((category, i) => {
							return <a className="itemlist-item" key={category._id} onClick={() => selectItem('update', category)}><span className="itemlist-num">{i}</span>{category.name}</a>;
						}) :
						recipes.map((recipe, i) => {
							return <a className="itemlist-item" key={recipe._id} onClick={() => selectItem('update', recipe)}><span className="itemlist-num">{i}</span>{recipe.name}</a>;
						})
					}
				</div>
			</div>
		);
	};
}

export default ItemList;
