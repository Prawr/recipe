// Main import
import React, { Component, Fragment } from 'react';

// Component import

// CSS import
import './creadit.css';

// Component
class CategoryContent extends Component {
  state = {
    header: '',
    category: {
      _id: null,
      name: '',
      active: false,
    },
  }

  getCategory(){
    this.props.goBack();
    return this.state.category;
  }

  onChange(event){
    let newCategory = this.state.category;
    newCategory.name = event.target.value;
    this.setState({
      category: newCategory,
    });
  }

  componentWillMount(){
    const { oldCategory } = this.props;
    const newHeader = oldCategory === null ? 'New Category' : 'Edit Category';
    this.setState({header: newHeader});
    if(oldCategory != null){
      this.setState({
        category: {
          _id: oldCategory._id, 
          name: oldCategory.name
        }
      });
    }
  }

	render(){
    const { saveCategory, oldCategory, goBack } = this.props;
		return (
      <Fragment>
        {/* Progress */}
        <div className="aside progress-list-container">
          <a className="progress-back-button" onClick={() => goBack()}><img src="/images/back-button-transparent.png" /></a>
          <ul className="progress-list">
            <li className="progress-button">Name</li>
          </ul>
        </div>
        {/* Content */}
        <div className="category-content">
          <div className="category-content-header">
            <h2>{this.state.header}</h2>
            <a className="category-content-save-button" onClick={() => saveCategory(this.getCategory())}>Save</a>
            <div className="category-content-underline"></div>
          </div>
          <div className="category-content-body">
            <div className="category-content-body-add">
              <input className="category-content-input" type="text" name="name" onChange={this.onChange.bind(this)} value={this.state.category.name} />
            </div>
          </div>
        </div>
      </Fragment>
    )
	}
};

export default CategoryContent;
