// Main import
import React from 'react';

export const BakingInfo = ({bakingInfo, currentBakingInfo, addItem, deleteItem, changeOrder, onChange, handleEnter}) => {
  return (
    <div className="category-content-body">
      <div className="category-content-body-add">
        <textarea rows="3" className="category-content-input" name="currentBakingInfo" onChange={onChange} onKeyPress={handleEnter} placeholder="Type baking information here..." value={currentBakingInfo} />
        <a className="add-button" onClick={() => addItem()}>Add</a>
      </div>
      <ul className="category-content-body-list">
        {bakingInfo.map((info, i) => {
          return (
            <li key={i}>
              {i == 0 ? (
                <span className="order-button-container">
                  <a className="order-button"><img src="/images/order-button-disabled.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'bakingInfo')}><img src="/images/order-button.png"/></a>
                </span>
              ) : i == bakingInfo.length-1 ?  (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'bakingInfo')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down"><img src="/images/order-button-disabled.png"/></a>
                </span>
              ) : (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'bakingInfo')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'bakingInfo')}><img src="/images/order-button.png"/></a>
                </span>
              )}
              {info}
              <a className="delete-button" onClick={() => deleteItem(i, 'bakingInfo')}><img src="/images/delete-button.png"/></a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}

export const Ingredients = ({ingredients, currentIngredient, addItem, deleteItem, changeOrder, onChange, handleEnter}) => {
  return (
    <div className="category-content-body">
      <div className="category-content-body-add">
        <textarea rows="1" className="category-content-input-s" name="amount" value={currentIngredient.amount} onChange={onChange} placeholder="0"/>
        <textarea rows="1" className="category-content-input-s" name="unit" value={currentIngredient.unit} onChange={onChange} placeholder="ml"/>
        <textarea rows="1" className="category-content-input-s" name="name" value={currentIngredient.name} onChange={onChange} onKeyPress={handleEnter} placeholder="Ingredient"/>
        <a className="add-button" onClick={() => addItem()}>Add</a>
      </div>
      <ul className="category-content-body-list">
        {ingredients.map((info, i) => {
          return (
            <li key={i}>
              {i == 0 ? (
                <span className="order-button-container">
                  <a className="order-button"><img src="/images/order-button-disabled.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'ingredients')}><img src="/images/order-button.png"/></a>
                </span>
              ) : i == ingredients.length-1 ?  (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'ingredients')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down"><img src="/images/order-button-disabled.png"/></a>
                </span>
              ) : (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'ingredients')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'ingredients')}><img src="/images/order-button.png"/></a>
                </span>
              )}
              {info.amount + ' '}
              {info.unit + ' '}
              {info.name}
              <a className="delete-button" onClick={() => deleteItem(i, 'ingredients')}><img src="/images/delete-button.png"/></a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}

export const Steps = ({steps, currentStep, addItem, deleteItem, changeOrder, onChange, handleEnter}) => {
  return (
    <div className="category-content-body">
      <div className="category-content-body-add">
        <textarea rows="3" className="category-content-input" name="currentStep" onChange={onChange} onKeyPress={handleEnter} placeholder="Type step here..." value={currentStep} />
        <a className="add-button" onClick={() => addItem()}>Add</a>
      </div>
      <ul className="category-content-body-list">
        {steps.map((info, i) => {
          return (
            <li key={i}>
              {i == 0 ? (
                <span className="order-button-container">
                  <a className="order-button"><img src="/images/order-button-disabled.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'steps')}><img src="/images/order-button.png"/></a>
                </span>
              ) : i == steps.length-1 ?  (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'steps')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down"><img src="/images/order-button-disabled.png"/></a>
                </span>
              ) : (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'steps')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'steps')}><img src="/images/order-button.png"/></a>
                </span>
              )}
              {info}
              <a className="delete-button" onClick={() => deleteItem(i, 'steps')}><img src="/images/delete-button.png"/></a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}

export const Tips = ({tips, currentTip, addItem, deleteItem, changeOrder, onChange, handleEnter}) => {
  return (
    <div className="category-content-body">
      <div className="category-content-body-add">
        <textarea rows="3" className="category-content-input" name="currentTip" onChange={onChange} onKeyPress={handleEnter} placeholder="Type tip here..." value={currentTip} />
        <a className="add-button" onClick={() => addItem()}>Add</a>
      </div>
      <ul className="category-content-body-list">
        {tips.map((info, i) => {
          return (
            <li key={i}>
              {i == 0 ? (
                <span className="order-button-container">
                  <a className="order-button"><img src="/images/order-button-disabled.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'tips')}><img src="/images/order-button.png"/></a>
                </span>
              ) : i == tips.length-1 ?  (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'tips')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down"><img src="/images/order-button-disabled.png"/></a>
                </span>
              ) : (
                <span className="order-button-container">
                  <a className="order-button" onClick={() => changeOrder(i, -1, 'tips')}><img src="/images/order-button.png"/></a>
                  <a className="order-button down" onClick={() => changeOrder(i, 1, 'tips')}><img src="/images/order-button.png"/></a>
                </span>
              )}
              {info}
              <a className="delete-button" onClick={() => deleteItem(i, 'tips')}><img src="/images/delete-button.png"/></a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}

export const Categories = ({categories, addItem, deleteItem, onChange, allCategories, recipeName, handleImage, imageState}) => {
  return (
    <div className="category-content-body">
      <div className="category-content-body-add">
        <textarea rows="1" className="category-content-input" name="recipeName" onChange={onChange} placeholder="Type name here..." value={recipeName} />
      </div>
      <div className="category-content-body-add">
        <input type="file" accept="image/*" name="image" onChange={handleImage}/>
        {imageState != null && imageState == false ? <span>File size must be under 1MB</span> : ''}
      </div>
      <div className="category-content-body-add">
        <select name="currentCategory" onChange={onChange} defaultValue="0">
          <option value="0" disabled>Select category</option>
          {allCategories.map((cat, i) => {
            return (
              <option key={i} value={cat._id}>{cat.name}</option>
            )
          })}
        </select>
        <a className="add-button" onClick={() => addItem()}>Add</a>
      </div>
      <ul className="category-content-body-list">
        {categories.map((info, i) => {
          return (
            <li key={i}>
              {allCategories.map((cat) => {
                if(cat._id == info){
                  return cat.name;
                }
              })}
              <a className="delete-button" onClick={() => deleteItem(i, 'categories')}><img src="/images/delete-button.png"/></a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}