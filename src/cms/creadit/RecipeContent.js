// Main import
import React, { Component, Fragment } from 'react';

// Component import
import { BakingInfo, Ingredients, Steps, Tips, Categories } from './RecipeParts';

// CSS import
import './creadit.css';

// Component
class RecipeContent extends Component {
  state = {
    header: '',
    recipe: {
      _id: null,
      name: '',
      image: null,
      bakingInfo: [],
      ingredients: [],
      steps: [],
      tips: [],
      categories: [],
    },
    currentBakingInfo: '',
    currentIngredient: '',
    currentStep: '',
    currentTip: '',
    currentCategory: '',
    image: null,
    bakingInfo: true,
    ingredients: false,
    steps: false,
    tips: false,
    categories: false,
  }

  onChange(event){
    if(this.state.ingredients == true){
      this.setState({
        currentIngredient: {
          ...this.state.currentIngredient,
          [event.target.name]: event.target.value,
        }
      });
    } else if (this.state.categories == true){
      if(event.target.name == 'recipeName'){
        this.setState({
          recipe:{
            ...this.state.recipe,
            name: event.target.value,
          }
        });
      } else {
        this.setState({
          [event.target.name]: event.target.options[event.target.options.selectedIndex].value,
        });
      }
    } else {
      this.setState({
        [event.target.name]: event.target.value,
      });
    }
  }

  handleEnter(event){
    if(event.key === 'Enter'){
      this.add();
      event.preventDefault();
    }
  }

  handleImage(event){
    const imageFile = event.target.files[0];
    // Check file size
    if(imageFile.size > 1000000){
      console.log(imageFile.size > 1000000);
      this.setState({ image: false });
      return;
    }

    const reader = new FileReader();
    reader.onload = () => {
      const dataURL = reader.result;
      this.setState({
        recipe: {
          ...this.state.recipe,
          image: dataURL,
        },
        image: true,
      });
    }
    reader.readAsDataURL(imageFile);
  }

  add(){
    const oldRecipe = this.state.recipe;
    if(this.state.bakingInfo == true){
      if(this.state.currentBakingInfo == '') return;
      let newBakingInfo = oldRecipe.bakingInfo;
      newBakingInfo.push(this.state.currentBakingInfo);
      this.setState({
        recipe: {
          ...oldRecipe,
          bakingInfo: newBakingInfo,
        },
        currentBakingInfo: '',
      });
    } else if(this.state.ingredients == true){
      if(this.state.currentIngredient.amount == '' || this.state.currentIngredient.unit == '' || this.state.currentIngredient.name == '') return;
      let newIngredients = oldRecipe.ingredients;
      newIngredients.push(this.state.currentIngredient);
      this.setState({
        recipe: {
          ...oldRecipe,
          ingredients: newIngredients,
        },
        currentIngredient: {
          amount: '',
          unit: '',
          name: '',
        },
      });
    } else if(this.state.steps == true){
      if(this.state.currentStep == '') return;
      let newSteps = oldRecipe.steps;
      newSteps.push(this.state.currentStep);
      this.setState({
        recipe: {
          ...oldRecipe,
          steps: newSteps,
        },
        currentStep: '',
      });
    } else if(this.state.tips == true){
      if(this.state.currentTip == '') return;
      let newTips = oldRecipe.tips;
      newTips.push(this.state.currentTip);
      this.setState({
        recipe: {
          ...oldRecipe,
          tips: newTips,
        },
        currentTip: '',
      });
    } else if(this.state.categories == true){
      if(this.state.currentCategory == '') return;
      let newCategories = oldRecipe.categories;
      newCategories.push(this.state.currentCategory);
      this.setState({
        recipe: {
          ...oldRecipe,
          categories: newCategories,
        }
      });
    } else {
      console.error('Adding unfamiliar object.');
    }
  }

  delete(key, target){
    const oldRecipe = this.state.recipe;
    let old;
    switch(target){
      case 'bakingInfo':
        old = oldRecipe.bakingInfo;break;
      case 'ingredients':
        old = oldRecipe.ingredients;break;
      case 'steps':
        old = oldRecipe.steps;break;
      case 'tips':
        old = oldRecipe.tips;break;
      case 'categories':
        old = oldRecipe.categories;break;
      default:
        console.error(target, ' is not supported.');
    }

    old.splice(key, 1);

    this.setState({
      recipe: {
        ...oldRecipe,
        [target]: old,
      }
    });
  }

  changeOrder(key, i, target){
    const oldRecipe = this.state.recipe;
    let newInfo = oldRecipe[target];
    let a = newInfo[key];
    let b = newInfo[key + i];

    newInfo[key] = b;
    newInfo[key + i] = a;

    this.setState({
      recipe: {
        ...oldRecipe,
        [target]: newInfo,
      }
    });
  }

  setActive(page){
    switch(page){
      case 'bakingInfo':
        this.setState({
          bakingInfo: true,
          ingredients: false,
          steps: false,
          tips: false,
          categories: false,
        });break;
      case 'ingredients':
        this.setState({
          bakingInfo: false,
          ingredients: true,
          steps: false,
          tips: false,
          categories: false,
        });break;
      case 'steps':
        this.setState({
          bakingInfo: false,
          ingredients: false,
          steps: true,
          tips: false,
          categories: false,
        });break;
      case 'tips':
        this.setState({
          bakingInfo: false,
          ingredients: false,
          steps: false,
          tips: true,
          categories: false,
        });break;
      case 'categories':
        this.setState({
          bakingInfo: false,
          ingredients: false,
          steps: false,
          tips: false,
          categories: true,
        });break;
      default:
        console.error(page, ' is not supported.');
    }
  }

  getRecipe(){
    this.props.goBack();
    return this.state.recipe;
  }

  componentWillMount(){
    const { oldRecipe } = this.props;
    const newHeader = oldRecipe === null ? 'New Recipe' : 'Edit Recipe';
    this.setState({header: newHeader});
    if(oldRecipe != null){
      this.setState({
        recipe: oldRecipe,
        image: true,
      });
    }
  }

	render(){
    const { goBack, allCategories, saveRecipe } = this.props;

    let currentBody;
    if(this.state.bakingInfo == true){
      currentBody = <BakingInfo bakingInfo={this.state.recipe.bakingInfo} onChange={this.onChange.bind(this)} currentBakingInfo={this.state.currentBakingInfo}
                      addItem={this.add.bind(this)} changeOrder={this.changeOrder.bind(this)} deleteItem={this.delete.bind(this)}
                      handleEnter={this.handleEnter.bind(this)} />;
    } else if(this.state.ingredients == true){
      currentBody = <Ingredients ingredients={this.state.recipe.ingredients} onChange={this.onChange.bind(this)} currentIngredient={this.state.currentIngredient}
                      addItem={this.add.bind(this)} changeOrder={this.changeOrder.bind(this)} deleteItem={this.delete.bind(this)}
                      handleEnter={this.handleEnter.bind(this)} />;
    } else if(this.state.steps == true){
      currentBody = <Steps steps={this.state.recipe.steps} onChange={this.onChange.bind(this)} currentStep={this.state.currentStep}
                      addItem={this.add.bind(this)} changeOrder={this.changeOrder.bind(this)} deleteItem={this.delete.bind(this)}
                      handleEnter={this.handleEnter.bind(this)} />;
    } else if(this.state.tips == true){
      currentBody = <Tips tips={this.state.recipe.tips} onChange={this.onChange.bind(this)} currentTip={this.state.currentTip}
                      addItem={this.add.bind(this)} changeOrder={this.changeOrder.bind(this)} deleteItem={this.delete.bind(this)}
                      handleEnter={this.handleEnter.bind(this)} />;
    } else if (this.state.categories == true){
      currentBody = <Categories categories={this.state.recipe.categories} onChange={this.onChange.bind(this)} recipeName={this.state.recipe.name}
                      addItem={this.add.bind(this)} deleteItem={this.delete.bind(this)} allCategories={allCategories}
                      handleImage={this.handleImage.bind(this)} imageState={this.state.image} />;
    } else {
      currentBody = <h1>NOT IMPLEMENTED</h1>;
    }

		return (
      <Fragment>
        {/* Progress */}
        <div className="progress-list-container">
          <a className="progress-back-button" onClick={() => goBack()}><img src="/images/back-button-transparent.png" /></a>
          <ul className="progress-list">
            <li className={this.state.bakingInfo == true ? "progress-button active" : this.state.recipe.bakingInfo.length > 0 ? "progress-button completed" : "progress-button"} onClick={() => this.setActive('bakingInfo')}>Baking Info</li>
            <li className={this.state.ingredients == true ? "progress-button active" : this.state.recipe.ingredients.length > 0 ? "progress-button completed" : "progress-button"} onClick={() => this.setActive('ingredients')}>Ingredients</li>
            <li className={this.state.steps == true ? "progress-button active" : this.state.recipe.steps.length > 0 ? "progress-button completed" : "progress-button"} onClick={() => this.setActive('steps')}>Steps</li>
            <li className={this.state.tips == true ? "progress-button active" : this.state.recipe.tips.length > 0 ? "progress-button completed" : "progress-button"} onClick={() => this.setActive('tips')}>Tips</li>
            <li className={this.state.categories == true ? "progress-button active" : this.state.recipe.categories.length > 0 ? "progress-button completed" : "progress-button"} onClick={() => this.setActive('categories')}>Categories</li>
          </ul>
        </div>
        {/* Content */}
        <div className="category-content">
          <div className="category-content-header">
            <h2>{this.state.header}</h2>
            {
              this.state.recipe.bakingInfo.length > 0 
                && this.state.recipe.ingredients.length > 0 
                && this.state.recipe.steps.length > 0
                && this.state.recipe.tips.length > 0
                && this.state.recipe.categories.length > 0
                && this.state.recipe.image != null
                && (this.state.image != null && this.state.image == true ) ? 
                  <a className="category-content-save-button" onClick={() => saveRecipe(this.getRecipe())}>Save</a> :
                  ''
            }
            <div className="category-content-underline"></div>
          </div>
          {/* Body */}
          {currentBody}
        </div>
      </Fragment>
    )
	}
};

export default RecipeContent;
