// import
import { TOGGLE_ACTIVE, REHYDRATE_CATEGORIES } from '../categories';

// Action
export const toggleActive = category => ({
  type: TOGGLE_ACTIVE,
  category
});

export const rehydrate = categories => ({
  type: REHYDRATE_CATEGORIES,
  categories
});
