// Import
import { SET_RECIPE, REHYDRATE_RECIPES } from '../recipes';

// Action
export const setRecipe = recipe => ({
  type: SET_RECIPE,
  recipe
});

export const rehydrate = recipes => ({
  type: REHYDRATE_RECIPES,
  recipes
});