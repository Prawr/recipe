// Main import
import { combineReducers } from 'redux';

// Reducer import
import categories from './categories';
import recipes from './recipes';

// Combine reducer
const recipeApp = combineReducers({categories, recipes});

export default recipeApp;
