// Action type
export const SET_RECIPE = 'SET_RECIPE';
export const REHYDRATE_RECIPES = 'REHYDRATE_RECIPES';

// Initial state
const INITIAL_STATE = {
  all: [],
  recipe: {}
};

// Reducer
export default (state = INITIAL_STATE, action) => {
  switch(action.type){
    case SET_RECIPE:
      return {
        ...state,
        recipe: action.recipe
      };
    case REHYDRATE_RECIPES:
      return {
        ...state,
        all: action.recipes 
      };
    default:
      return state;
  }
}
