// Action type
export const TOGGLE_ACTIVE = 'TOGGLE_ACTIVE';
export const REHYDRATE_CATEGORIES = 'REHYDRATE_CATEGORIES';

// Initial state
const INITIAL_STATE = [];

// Reducer
export default (state = INITIAL_STATE, action) => {
  switch(action.type){
    case TOGGLE_ACTIVE:
      return state.map(category => {
        if(category.name === action.category.name){
          return { ...category, active: !category.active };
        }
        return category;
      });
    case REHYDRATE_CATEGORIES:
      return action.categories;
    default:
      return state;
  }
}
