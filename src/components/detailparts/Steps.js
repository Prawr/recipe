// Main import
import React from 'react';

// CSS import
import './detailparts.css';

// Component
const Steps = ({steps}) => {
  return (
    <div className="recipe-steps">
      <h2 className="subtitle">Stappen</h2>
      <div className="underline"></div>
      <ol className="detail-list">
        {
          steps ?
            steps.map((step,i) => {
              return <li key={i}>{step}</li>
            }) :
            <p>Geen stappen beschikbaar.</p>
        }
      </ol>
      <div className="divider"></div>
    </div>
  )
}

export default Steps;