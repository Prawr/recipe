// Main import
import React from 'react';

// CSS import
import './detailparts.css';

// Component
const BakingInfo = ({bakingInfo}) => {
  return (
    <div className="recipe-bakinginfo">
      <h2 className="subtitle">Bak Informatie</h2>
      <div className="underline"></div>
      <ul className="detail-list">
        {
          bakingInfo != undefined ?
          bakingInfo.map((info, i) => {
            return <li key={i}>{info}</li>
          }) :
          <p>Geen bak informatie beschikbaar.</p>
        }
      </ul>
      <div className="divider"></div>
    </div>
  )
}

export default BakingInfo;