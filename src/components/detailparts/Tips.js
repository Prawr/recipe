// Main import
import React from 'react';

// CSS import
import './detailparts.css';

// Component
const Tips = ({tips}) => {
  return (
    <div className="recipe-tips">
      <h2 className="subtitle">Tips</h2>
      <div className="underline"></div>
      <ul className="detail-list">
        {
          tips ?
            tips.map((tip, i) => {
              return <li key={i}>{tip}</li>
            }) :
            <p>Geen tips beschikbaar.</p>
        }
      </ul>
    </div>
  )
}

export default Tips;