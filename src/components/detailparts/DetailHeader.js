// Main import
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// CSS import
import './detailparts.css';

// Component
const DetailHeader = ({recipe}) => {
  return (
    <div className="detail-header">
      <Link to="/" className="back-button"><img src="/images/back-button-transparent.png" /></Link>
      <div className="header-content">
        <img src={recipe.image == null ? "http://via.placeholder.com/1440x720" : recipe.image} />
        <h1 className="title">{recipe.name}</h1>
      </div>
      <div className="divider"></div>
    </div>
  )
};

export default DetailHeader;