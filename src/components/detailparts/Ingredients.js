// Main import
import React from 'react';

// CSS import
import './detailparts.css';

// Component
const Ingredients = ({ingredients}) => {
  // Create second array for the right half of the ingredients list.
  let ingredients1, ingredients2;
  if(ingredients && ingredients.length > 5){
    ingredients1 = ingredients.slice(ingredients.length/2, ingredients.length);
    ingredients2 = ingredients.slice(0, ingredients.length/2);
  }
  
  return (
    <div className="recipe-ingredients">
      <h2 className="subtitle">Ingredi&euml;nten</h2>
      <div className="underline"></div>
      {/* Ingredients lists */}
        {
          ingredients1 != undefined && ingredients2 != undefined ? (
            <div className="ingredient-container">
              <ul className="detail-list split">
              {ingredients2.map((ingredient, i) => {
                return (
                  <li key={i}>
                    <span>{ingredient.amount} {ingredient.unit}</span>
                    <span>{ingredient.name}</span>
                  </li>
                )
              })}
            </ul>
            <div className="vertical-divider"></div>
            <ul className="detail-list split">
              {ingredients1.map((ingredient, i) => {
                return (
                  <li key={i}>
                    <span>{ingredient.amount} {ingredient.unit}</span>
                    <span>{ingredient.name}</span>
                  </li>
                )
              })}
            </ul>
          </div>) : ingredients != undefined ? (
            <div className="ingredient-container">
              <ul className="detail-list split">
              {ingredients.map((ingredient, i) => {
                return (
                  <li key={i}>
                    <span>{ingredient.amount} {ingredient.unit}</span>
                    <span>{ingredient.name}</span>
                  </li>
                )
              })}
            </ul>
          </div>) :
          <p style={{marginLeft: 40 + 'px'}}>Geen ingrediënten beschikbaar.</p>
        }
      <div className="divider"></div>
    </div>
  )
}

export default Ingredients;