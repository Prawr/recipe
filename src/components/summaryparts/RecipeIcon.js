// Main import
import React from 'react';
import { Link } from 'react-router-dom';

// CSS import
import './summaryparts.css';

// Component
const RecipeIcon = ({recipe, handleSetRecipe}) => {
  return (
    <Link to="/detail" className="button recipe-icon" onClick={handleSetRecipe(recipe)}>
      <img src={recipe.image == null ? "http://via.placeholder.com/720x720" : recipe.image} />
      {recipe.name}
    </Link>
  )
}

export default RecipeIcon;