// Main import
import React, { Component } from 'react';

// CSS import
import './summaryparts.css';

// Component
class CategoryList extends Component {
	handleToggleActive = category => {
		return () => this.props.catActions.toggleActive(category);
	};

	render(){
		const { categories } = this.props;

		return (
			<div className="aside">
				{categories.map((cat, i) => {
					return <a key={i} className={cat.active ? "button category-button active" : "button category-button"} onClick={this.handleToggleActive(cat)}>{cat.name}</a>;
				})}
			</div>
		);
	}
};

export default CategoryList;
