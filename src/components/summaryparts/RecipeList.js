// Main import
import React, { Component } from 'react';

// Component import
import RecipeIcon from './RecipeIcon';

// Css import
import './summaryparts.css';

// Component
class RecipeList extends Component {
	handleSetRecipe = rec => {
		return () => this.props.recActions.setRecipe(rec);
	};

	render(){
		const { recipes, categories } = this.props;

		// Get all the active categories to filter on.
		let activeCategories = [];
		categories.map(cat => {
			if(cat.active){
				activeCategories.push(cat._id);
			}
		});

		// Method to check whether all the categories of the recipe are active.
		const shouldShow = (recipeCategories) => {
			let i = 0;
			activeCategories.map((activeCat) => {
				if(recipeCategories.includes(activeCat)){
					i++;
				}
			});
			return activeCategories.length == i;
		}

		return (
			<div className="content recipe-list">
        {recipes.map(rec => {
					if(shouldShow(rec.categories)){
						return <RecipeIcon key={rec._id} recipe={rec} handleSetRecipe={this.handleSetRecipe} />
					}
				})}
			</div>
		);
	}
};

export default RecipeList;
