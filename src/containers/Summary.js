// Main import
import React from 'react';

// Component import
import CategoryList from '../components/summaryparts/CategoryList';
import RecipeList from '../components/summaryparts/RecipeList';

// Component
const Summary = ({categories, catActions, recipes, recActions}) => (
	<div className="summarypage">
		<CategoryList categories={categories} catActions={catActions} />
		<RecipeList categories={categories} recipes={recipes} recActions={recActions} />
	</div>
);

export default Summary;
