// Main import
import React, { Component } from 'react';

// Component import
import DetailHeader from '../components/detailparts/DetailHeader';
import BakingInfo from '../components/detailparts/BakingInfo';
import Ingredients from '../components/detailparts/Ingredients';
import Steps from '../components/detailparts/Steps';
import Tips from '../components/detailparts/Tips';

// Component
class Detail extends Component {
  state = {
    recipe: null,
  }

  setSessionRecipe = recipe => {
	  sessionStorage.setItem("recipe", JSON.stringify(recipe));
  }

  componentWillMount(){
    const { recipe } = this.props;

    if(recipe.name == null){
      const sessionRecipe = JSON.parse(sessionStorage.getItem("recipe"));
      this.setState({recipe: sessionRecipe});
      console.log(sessionRecipe);
    } else {
      this.setState({recipe: recipe});
      this.setSessionRecipe(recipe);
    }
  }

  render(){
    const recipe = this.state.recipe;

    return (
      <div className="detailpage">
        <DetailHeader recipe={recipe} />
        <BakingInfo bakingInfo={recipe.bakingInfo} />
        <Ingredients ingredients={recipe.ingredients} />
        <Steps steps={recipe.steps} />
        <Tips tips={recipe.tips} />
      </div>
    );
  }
}

export default Detail;
