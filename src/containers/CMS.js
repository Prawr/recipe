// Main import
import React, { Component } from 'react';

// Component import
import Summary from '../cms/summary/Summary';

// Component
class CMS extends Component {
  host = '192.168.178.31';
  saveCategory(category){
    console.info('CMS - saving category: ',JSON.stringify(category));
    const url = category._id == null ? 'http://'+ this.host +':8081/categories' : 'http://'+ this.host +':8081/categories/' + category._id;
    fetch(url, {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(category)})
      .then((response) => response.json())
      .then(json => console.info('CMS - response: ', json))
      .then(this.props.rehydrateCategories());
  }

  saveRecipe(recipe){
    console.info('CMS - saving recipe: ', JSON.stringify(recipe));
    const url = recipe._id == null ? 'http://'+ this.host +':8081/recipes' : 'http://'+ this.host +':8081/recipes/' + recipe._id;
    fetch(url, {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(recipe)})
      .then((response) => response.json())
      .then(json => console.info('CMS - response: ', json))
      .then(this.props.rehydrateRecipes());
  }

  render(){
    const { categories, recipes } = this.props;
    return (
      <Summary categories={categories} recipes={recipes} saveCategory={this.saveCategory.bind(this)} saveRecipe={this.saveRecipe.bind(this)} />
    );
  }
}

export default CMS;
