// Main import
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Switch, Route, withRouter } from 'react-router-dom';

// Container import
import Summary from '../Summary';
import Detail from '../Detail';
import CMS from '../CMS';

// Reducer import
import * as categoryActions from '../../reducers/actions/categories';
import * as recipeActions from '../../reducers/actions/recipes';

// CSS import
import './app.css';

// Component
class App extends Component {
	host = '192.168.178.31';
	rehydrateCategories(){
		console.info('App - rehydrating categories');
		fetch('http://'+ this.host +':8081/categories', {method: 'get'})
			.then(response => response.json())
			.then(json => {
				if(json.length != 0){
					this.props.catActions.rehydrate(json);
				}
			});
	}

	rehydrateRecipes(){
		console.info('App - rehydrating recipes');
		fetch('http://'+ this.host +':8081/recipes', {method: 'get'})
			.then(response => response.json())
			.then(json => {
				if(json.length != 0){
					this.props.recActions.rehydrate(json);
				}
			});
	}

	componentWillMount(){
		this.rehydrateCategories();
		this.rehydrateRecipes();
	}

  render(){
		const { categories, catActions, recipes, recActions, recipe } = this.props;
		return (
			<Switch>
				<Route exact path='/' render={() => <Summary categories={categories} catActions={catActions} recipes={recipes} recActions={recActions} /> } />
				<Route exact path='/detail' render={() => <Detail recipe={recipe} /> } />
				<Route path='/cms' render={() => <CMS categories={categories} recipes={recipes} rehydrateCategories={() => this.rehydrateCategories.bind(this)} rehydrateRecipes={() => this.rehydrateRecipes.bind(this)} />} />
			</Switch>
		);
  }
}

// Prop mapping
const mapStateToProps = state => ({
	categories: state.categories,
	recipes: state.recipes.all,
	recipe: state.recipes.recipe,
});

const mapDispatchToProps = dispatch => ({
	catActions: bindActionCreators(categoryActions, dispatch),
	recActions: bindActionCreators(recipeActions, dispatch),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
