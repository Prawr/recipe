// Main import
import React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore } from 'redux';

// Component import
import App from './containers/app/App';
import recipeApp from './reducers/reducer';

// Create the store
let store = createStore(recipeApp);

window.store = store;

// Render the App
render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
