const express = require('express'),
  app = express(),
  port = process.env.API_PORT;

const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const Category = require("./model/CategoryModel");
const Recipe = require("./model/RecipeModel");
const catRoutes = require('./route/CategoryRoute');
const recRoutes = require('./route/RecipeRoute');

const dbConn = 'mongodb+srv://' + process.env.DB_USER + ':' + process.env.DB_PASS + '@' + process.env.DB_HOST;
mongoose.connect(dbConn , {useNewUrlParser: true});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '2mb'}));
catRoutes(app);
recRoutes(app);

app.listen(port);
console.log("Recipe server listening at :" + port);