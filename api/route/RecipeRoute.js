module.exports = function(app){
  const recipe = require('../controller/RecipeController');

  app.route('/recipes')
    .get(recipe.all)
    .post(recipe.create)
    .options(recipe.confirm);

  app.route('/recipes/:id')
    .post(recipe.update)
    .options(recipe.confirm);
}