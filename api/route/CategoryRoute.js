module.exports = function(app){
  const category = require('../controller/CategoryController');

  app.route('/categories')
    .get(category.all)
    .post(category.create)
    .options(category.confirm);

  app.route('/categories/:id')
    .post(category.update)
    .options(category.confirm);
}