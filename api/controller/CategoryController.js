var mongoose = require('mongoose'),
  Category = mongoose.model('Category'),
  whitelistip = process.env.WHITELIST_IP;

// Exports
exports.confirm = (req, res) => {
  res.header('Access-Control-Allow-Origin', whitelistip);
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.send(req.body);
}

exports.all = (req, res) => {
  Category.find({}, (err, category) => {
    if(err) res.send(err);
    res.header('Access-Control-Allow-Origin', whitelistip);
    res.json(category);
  });
};

exports.create = (req, res) => {
  const newCategory = new Category(req.body);
  newCategory.save((err, category) => {
    if(err) res.send(err);
    res.header('Access-Control-Allow-Origin', whitelistip);
    res.json(category);
  });
};

exports.update = (req, res) => {
  Category.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, (err, category) => {
    if(err) res.send(err);
    res.header('Access-Control-Allow-Origin', whitelistip);
    res.json(category);
  });
};