var mongoose = require('mongoose'),
  Recipe = mongoose.model('Recipe'),
  whitelistip = process.env.WHITELIST_IP;

// Exports
exports.confirm = (req, res) => {
  res.header('Access-Control-Allow-Origin', whitelistip);
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.send(req.body);
}

exports.all = (req, res) => {
  Recipe.find({}, (err, recipe) => {
    if(err) res.send(err);
    res.header('Access-Control-Allow-Origin', whitelistip);
    res.json(recipe);
  });
};

exports.create = (req, res) => {
  const newRecipe = new Recipe(req.body);
  newRecipe.save((err, recipe) => {
    if(err) res.send(err);
    res.header('Access-Control-Allow-Origin', whitelistip);
    res.json(recipe);
  });
};

exports.update = (req, res) => {
  Recipe.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, (err, recipe) => {
    if(err) res.send(err);
    res.header('Access-Control-Allow-Origin', whitelistip);
    res.json(recipe);
  });
};