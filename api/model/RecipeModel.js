const mongoose = require('mongoose');
const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const RecipeSchema = new Schema({
  name: String,
  image: String,
  bakingInfo: Array,
  ingredients: Array,
  steps: Array,
  tips: Array,
  categories: Array
});

module.exports = mongoose.model('Recipe', RecipeSchema);