const mongoose = require('mongoose');
const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const CategorySchema = new Schema({
  name: String,
  active: Boolean
});

module.exports = mongoose.model('Category', CategorySchema);